filename=fiatlux

pdf:
	pdflatex ${filename}

html:
	htlatex ${filename}

clean:
	rm -f ${filename}.{pdf,ps,log,aux,out,dvi,bbl,blg,4ct,4tc,css,html,idv,lg,tmp,xref}
